﻿namespace BlackSound.Models.Bindinig.Song
{
    using System.ComponentModel.DataAnnotations;

    public class SongBindingModel
    {
        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [MinLength(3)]
        public string Artists { get; set; }
        
        public int Year { get; set; }
    }
}
