﻿namespace BlackSound.Models.Bindinig.Song
{
    public class SongFilterBindingModel : IPageFilter
    {
        public int Page { get; set; } = 1;
        public int ItemsPerPage { get; set; }
    }
}
