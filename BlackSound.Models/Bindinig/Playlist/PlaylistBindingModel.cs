﻿namespace BlackSound.Models.Bindinig.Playlist
{
    using System.ComponentModel.DataAnnotations;

    public class PlaylistBindingModel
    {
        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsPublic { get; set; }
    }
}
