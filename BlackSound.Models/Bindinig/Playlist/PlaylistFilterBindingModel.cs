﻿namespace BlackSound.Models.Bindinig.Playlist
{
    public class PlaylistFilterBindingModel : IPageFilter
    {
        public int Page { get; set; } = 1;
        public int ItemsPerPage { get; set; }
        public int UserId { get; set; }
    }
}
