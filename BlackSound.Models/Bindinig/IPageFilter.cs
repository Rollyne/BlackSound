﻿namespace BlackSound.Models.Bindinig
{
    public interface IPageFilter
    {
        int Page { get; set; }
        int ItemsPerPage { get; set; }
    }
}
