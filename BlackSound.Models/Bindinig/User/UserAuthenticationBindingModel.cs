﻿namespace BlackSound.Models.Bindinig.User
{
    using System.ComponentModel.DataAnnotations;

    public class UserAuthenticationBindingModel
    {
        [Required]
        [MinLength(3)]
        [EmailAddress]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Password { get; set; }
    }
}
