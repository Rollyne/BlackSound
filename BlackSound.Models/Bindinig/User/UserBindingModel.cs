﻿namespace BlackSound.Models.Bindinig.User
{
    using System.ComponentModel.DataAnnotations;

    public class UserBindingModel
    {
        [Required]
        [MinLength(3)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Password { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string DisplayName { get; set; }
    }
}
