﻿namespace BlackSound.Models.View.Song
{
    public class SongDetailsViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Artists { get; set; }

        public int Year { get; set; }
    }
}
