﻿namespace BlackSound.Models.View.Playlist
{
    public class PlaylistDetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsPublic { get; set; }
    }
}
