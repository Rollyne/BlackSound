﻿namespace BlackSound.Models.View.Playlist
{
    public class PlaylistIndexViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsPublic { get; set; }

        public override string ToString()
        {
            return $"{Id}| {Name} - Public: {IsPublic}";
        }
    }
}
