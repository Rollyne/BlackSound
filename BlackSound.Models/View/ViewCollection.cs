﻿namespace BlackSound.Models.View
{
    using System.Text;
    using System.Collections;
    using System.Collections.Generic;
    using Playlist;

    public class ViewCollection<T> : ICollection<T>
    {
        private ICollection<T> _collection;

        public ViewCollection(ICollection<T> collection)
        {
            _collection = collection;
        }

        public ViewCollection()
        {
            _collection = new List<T>();
        }

        public void SetCollection(ICollection<T> collection)
        {
            _collection = collection;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            _collection.Add(item);
        }

        public void Clear()
        {
            _collection.Clear();
        }

        public bool Contains(T item)
        {
            return _collection.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _collection.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return _collection.Remove(item);
        }

        public int Count => _collection.Count;
        public bool IsReadOnly => _collection.IsReadOnly;

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var item in _collection)
            {
                sb.Append(item);
            }

            return sb.ToString();
        }
    }
}
