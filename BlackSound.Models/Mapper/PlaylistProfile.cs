﻿namespace BlackSound.Models.Mapper
{
    using Entities;
    using View.Playlist;
    using AutoMapper;
    using System.Collections.Generic;
    using View;
    using View.Song;
    using Bindinig.Playlist;

    public class PlaylistProfile : Profile
    {
        public PlaylistProfile()
        {
            CreateMap<Playlist, PlaylistIndexViewModel>();
            CreateMap<Playlist, PlaylistDetailsViewModel>();
            CreateMap<PlaylistBindingModel, Playlist>();
            CreateMap<Playlist, PlaylistBindingModel>();
        }
    }
}
