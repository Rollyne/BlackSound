﻿
namespace BlackSound.Models.Mapper
{
    using AutoMapper;
    using Bindinig.User;
    using Entities;
    using View;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper.QueryableExtensions;
    using View.Playlist;
    using View.User;

    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserBindingModel, User>();
            CreateMap<User, UserBindingModel>();
            CreateMap<UserRegisterBindingModel, User>();
            CreateMap<User, UserIndexViewModel>();
            CreateMap<User, UserDetailsViewModel>();


        }
    }
}
