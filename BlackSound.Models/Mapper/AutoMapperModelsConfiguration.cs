﻿namespace BlackSound.Models.Mapper
{
    public static class AutoMapperModelsConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<SongProfile>();
                cfg.AddProfile<PlaylistProfile>();
            });
        }
    }
}
