﻿namespace BlackSound.Models.Mapper
{
    using Bindinig.Song;
    using Entities;
    using View.Song;
    using AutoMapper;

    public class SongProfile : Profile
    {
        public SongProfile()
        {
            CreateMap<Song, SongIndexViewModel>();
            CreateMap<Song, SongDetailsViewModel>();
            CreateMap<SongBindingModel, Song>();
            CreateMap<Song, SongBindingModel>();
            CreateMap<SongPlaylist, Song>();
            CreateMap<SongPlaylist, SongIndexViewModel>();
        }
    }
}
