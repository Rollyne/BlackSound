﻿namespace BlackSound.Models.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Song : IIdentificatable
    {
        private ICollection<SongPlaylist> _playlists;
        public Song()
        {
            _playlists = new HashSet<SongPlaylist>();
        }
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [MinLength(3)]
        public string Artists { get; set; }

        public int Year { get; set; }

        public virtual ICollection<SongPlaylist> Playlists
        {
            get { return _playlists; }
            set { _playlists = value; }
        }
    }
}
