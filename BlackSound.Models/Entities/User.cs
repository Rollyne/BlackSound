﻿namespace BlackSound.Models.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class User : IIdentificatable
    {
        private ICollection<Playlist> _playlists;

        public User()
        {
            _playlists = new HashSet<Playlist>();
        }

        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Password { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string DisplayName { get; set; }

        public bool IsAdministrator { get; set; }

        public virtual ICollection<Playlist> Playlists
        {
            get { return _playlists; }
            set { _playlists = value; }
        }
    }
}
