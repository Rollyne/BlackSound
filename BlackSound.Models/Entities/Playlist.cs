﻿namespace BlackSound.Models.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Playlist : IIdentificatable
    {
        private ICollection<SongPlaylist> _songs;

        public Playlist()
        {
            _songs = new HashSet<SongPlaylist>();
        }
        
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Name { get; set; }
        
        public string Description { get; set; }

        [ForeignKey("Creator")]
        public int CreatorId { get; set; }
        public User Creator { get; set; }

        public bool IsPublic { get; set; }

        public virtual ICollection<SongPlaylist> Songs
        {
            get { return _songs; }
            set { _songs = value; }
        }

    }
}
