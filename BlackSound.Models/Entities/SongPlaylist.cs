﻿namespace BlackSound.Models.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class SongPlaylist : IIdentificatable
    {
        public int Id { get; set; }

        [Key, Column(Order = 0), ForeignKey("Playlist")]
        public int PlaylistId { get; set; }
        public virtual Playlist Playlist { get; set; }

        [Key, Column(Order = 1), ForeignKey("Song")]
        public int SongId { get; set; }
        public virtual Song Song { get; set; }
    }
}
