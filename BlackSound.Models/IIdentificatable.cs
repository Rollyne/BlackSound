﻿namespace BlackSound.Models
{
    public interface IIdentificatable
    {
        int Id { get; set; }
    }
}
