﻿namespace BlackSound.Services
{
    using System;
    using Data.Repositories.Tools;
    using Data.UnitOfWork;
    using Models.Entities;
    using Models.View.Song;
    using Tools;
    using Tools.Generic;

    public class SongsPlaylistsService : Service<UnitOfWork>
    {
        public IExecutionResult AddSongToPlaylist(int songId, int playlistId)
        {
            var repo = UnitOfWork.GetRepository<SongPlaylist>();
            var execution = new ExecutionResult();
            try
            {
                var song = UnitOfWork.GetRepository<Song>().FirstOrDefault(@where: c => c.Id == songId);
                if (song == null)
                {
                    execution.Succeeded = false;
                    execution.Message = CrudMessages.NotFound("song");
                    return execution;
                    
                }
                var item = new SongPlaylist()
                {
                    SongId = songId,
                    PlaylistId = playlistId,
                    Song = song,
                    Playlist = UnitOfWork.GetRepository<Playlist>().FirstOrDefault(s => s.Id == playlistId)
                };
                
                repo.AddOrUpdate(item);

                UnitOfWork.Save();
                execution.Succeeded = true;
                execution.Message = SongPlaylistMessages.SuccessfullyAdded();
            }
            catch (Exception)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }
            return execution;
        }

        public IExecutionResult<bool> IsAlreadyInThePlaylist(int playlistId, int songId)
        {
            var repo = UnitOfWork.GetRepository<SongPlaylist>();
            var execution = new ExecutionResult<bool>();

            try
            {
                if (repo.Any(i => i.PlaylistId == playlistId && i.SongId == songId))
                {
                    execution.Succeeded = true;
                    execution.Result = true;
                    execution.Message = SongPlaylistMessages.AlreadyInPlaylist();
                    return execution;
                }
            }
            catch (Exception)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }
            return execution;
        }

        public IExecutionResult RemoveSongFromPlaylist(int songId, int playlistId)
        {
            var repo = UnitOfWork.GetRepository<SongPlaylist>();
            var execution = new ExecutionResult();
            try
            {
                var item = repo.FirstOrDefault(where: i => i.SongId == songId && i.PlaylistId == playlistId);
                if (item == null)
                {
                    execution.Succeeded = false;
                    execution.Message = SongPlaylistMessages.NotInPlaylist();
                    return execution;
                }
                repo.Delete(item);
                UnitOfWork.Save();
                UnitOfWork.Save();

                execution.Succeeded = true;
                execution.Message = SongPlaylistMessages.SuccessfullyRemoved();
            }
            catch (Exception)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }

            return execution;
        }
    }
}