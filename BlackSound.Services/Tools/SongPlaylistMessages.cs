﻿namespace BlackSound.Services.Tools
{
    public static class SongPlaylistMessages
    {
        public static string SuccessfullyAdded() => "The song was successfully added to the playlist.";
        public static string SuccessfullyRemoved() => "The song was successfully removed from the playlist.";
        public static string NotInPlaylist() => "This song is not in the playlist.";
        public static string AlreadyInPlaylist() => "This song is already in the playlist.";
    }
}
