﻿namespace BlackSound.Services.Tools
{
    public interface IExecutionResult
    {
        bool Succeeded { get; set; }

        string Message { get; set; }
    }
}
