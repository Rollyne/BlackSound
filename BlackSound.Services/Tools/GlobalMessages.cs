﻿namespace BlackSound.Services.Tools
{
    public static class GlobalMessages
    {
        public static string InternalError()
            => "There was an internal server error. Please report the steps you took to this error.";

        public static string UserFound() => "Your profile was found successfully.";

        public static string SuccessfullyRegistered() => "You were successfully registered.";
    }
}
