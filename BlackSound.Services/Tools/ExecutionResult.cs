﻿namespace BlackSound.Services.Tools
{
    public class ExecutionResult : IExecutionResult
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
    }
}
