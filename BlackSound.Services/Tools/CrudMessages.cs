﻿namespace BlackSound.Services.Tools
{
    public static class CrudMessages
    {
        public static string SuccsessfullyLoadedItems(int amount, int outOf, string items = "items")
            => $"Succcessfully loaded {amount} out of {outOf} {items}";

        public static string NotFound(string item = "item") => $"The {item} you are looking for was not found.";

        public static string SuccessfullyLoadedDetails(string item = "item") => $"The details for this {item} were successfully loaded.";

        public static string SuccessfullChangeApply(string item = "item") => $"Your changes to this {item} were successfully applied";

        public static string SuccesufullyDeleted(string item = "item") => $"You succsessfully deleted this {item}";

        public static string SuccessfullyLoadedForModification(string item = "item")
            => $"This {item} was successfully loaded for modification.";

        public static string NoAccess(string item = "item") => $"You do not have access to this {item}.";
    }
}
