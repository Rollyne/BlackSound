﻿namespace BlackSound.Services
{
    using Data.Repositories.Tools;
    using Data.UnitOfWork;
    using Models.Bindinig.Playlist;
    using Models.Entities;
    using Models.View.Playlist;
    using Tools.Generic;

    public class PlaylistsService : CrudService<UnitOfWork, Playlist, PlaylistIndexViewModel, PlaylistBindingModel, PlaylistDetailsViewModel, PlaylistFilterBindingModel>
    {
        public bool HasAccess(int id, int userId)
        {
            return UnitOfWork.GetRepository<Playlist>().Any(i => i.Id == id && (i.IsPublic || i.CreatorId == userId));
        }

        public bool IsCreator(int id, int userId)
        {
            return UnitOfWork.GetRepository<Playlist>().Any(i => i.Id == id && (i.CreatorId == userId));
        }

        public override IExecutionResult<IPager<PlaylistIndexViewModel>> GetAllFiltered(PlaylistFilterBindingModel filter)
        {
            return _getAllFiltered(filter, where: i => i.IsPublic || i.CreatorId == filter.UserId);
        }
    }
}
