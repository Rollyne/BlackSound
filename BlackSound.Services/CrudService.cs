﻿

namespace BlackSound.Services
{
    using System;
    using System.Data.Entity.Validation;
    using System.Linq.Expressions;
    using System.Text;
    using Data.Repositories.Tools;
    using Data.UnitOfWork;
    using Models;
    using Models.Bindinig;
    using Tools;
    using Tools.Generic;

    public class CrudService<TUnitOfWork, TEntity, TIndexViewModel, TBindingModel, TDetailsViewModel, TFilterBindingModel> 
        : Service<TUnitOfWork>, ICrudService<TEntity, TIndexViewModel, TBindingModel, TDetailsViewModel, TFilterBindingModel> 
        where TUnitOfWork : IUnitOfWork, new()
        where TEntity : class, IIdentificatable
        where TFilterBindingModel : IPageFilter
    {
        protected virtual IExecutionResult<IPager<TIndexViewModel>> _getAllFiltered(TFilterBindingModel filter,
            Expression<Func<TEntity, bool>> where)
        {
            var repo = UnitOfWork.GetRepository<TEntity>();
            var execution = new ExecutionResult<IPager<TIndexViewModel>>();
            try
            {
                execution.Result = repo.GetAllPaged<TIndexViewModel>(filter.Page, filter.ItemsPerPage, where: where);
                if (execution.Result.Model != null)
                {
                    execution.Succeeded = true;
                    execution.Message = CrudMessages.SuccsessfullyLoadedItems(execution.Result.Model.Count,
                        execution.Result.ItemsAvaliable);
                }
                else
                {
                    execution.Succeeded = false;
                    execution.Message = CrudMessages.NotFound("page");
                }
            }
            catch (Exception e)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }

            return execution;
        }

        //This should be overrided if there are any search options to the filter model
        public virtual IExecutionResult<IPager<TIndexViewModel>> GetAllFiltered(TFilterBindingModel filter)
        {
            return _getAllFiltered(filter, where: i => true);
        }

        public IExecutionResult<TDetailsViewModel> GetDetails(int id)
        {
            var repo = UnitOfWork.GetRepository<TEntity>();
            var execution = new ExecutionResult<TDetailsViewModel>();
            try
            {
                execution.Result = repo.FirstOrDefault<TDetailsViewModel>(where: i => i.Id == id);
                if (execution.Result == null)
                {
                    execution.Message = CrudMessages.NotFound(typeof(TEntity).Name.ToLower());
                    execution.Succeeded = false;
                }

                execution.Succeeded = true;
                execution.Message = CrudMessages.SuccessfullyLoadedDetails(typeof(TEntity).Name.ToLower());
            }
            catch (Exception e)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }

            return execution;
        }

        public IExecutionResult AddOrUpdate(TEntity item)
        {
            var execution = new ExecutionResult<TEntity>();

            try
            {
                var repo = UnitOfWork.GetRepository<TEntity>();
                repo.AddOrUpdate(item);
                UnitOfWork.Save();
                execution.Succeeded = true;
                execution.Message = CrudMessages.SuccessfullChangeApply(typeof(TEntity).Name.ToLower());
            }
            catch (DbEntityValidationException e)
            {
                var sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine($"{ve.PropertyName}: \"{ve.ErrorMessage}\"");
                    }
                }
                execution.Succeeded = false;
                execution.Message = sb.ToString();
            }
            catch (Exception e)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }

            return execution;
        }

        public IExecutionResult Delete(int id)
        {
            var repo = UnitOfWork.GetRepository<TEntity>();
            var execution = new ExecutionResult<TEntity>();
            var item = repo.FirstOrDefault(where: i => i.Id == id);
            if (item == null)
            {
                execution.Message = CrudMessages.NotFound(typeof(TEntity).Name.ToLower());
                execution.Succeeded = false;
            }
            else
            {
                try
                {
                    repo.Delete(item);
                    UnitOfWork.Save();
                    execution.Succeeded = true;
                    execution.Message = CrudMessages.SuccesufullyDeleted(typeof(TEntity).Name.ToLower());
                }
                catch (DbEntityValidationException e)
                {
                    var sb = new StringBuilder();
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sb.AppendLine($"{ve.PropertyName}: \"{ve.ErrorMessage}\"");
                        }
                    }
                    execution.Succeeded = false;
                    execution.Message = sb.ToString();
                }
                catch (Exception)
                {
                    execution.Succeeded = false;
                    execution.Message = GlobalMessages.InternalError();
                }
            }
            

            return execution;
        }

        public IExecutionResult<TBindingModel> GetForModification(int id)
        {
            var repo = UnitOfWork.GetRepository<TEntity>();
            var execution = new ExecutionResult<TBindingModel>();
            try
            {
                execution.Result = repo.FirstOrDefault<TBindingModel>(where: i => i.Id == id);
                if (execution.Result == null)
                {
                    execution.Message = CrudMessages.NotFound(typeof(TEntity).Name.ToLower());
                    execution.Succeeded = false;
                    return execution;
                }

                execution.Succeeded = true;
                execution.Message = CrudMessages.SuccessfullyLoadedForModification(typeof(TEntity).Name.ToLower());
            }
            catch (Exception)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }

            return execution;
        }
    }
}
