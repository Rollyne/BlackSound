﻿

namespace BlackSound.Services
{
    using Data.UnitOfWork;
    using Models.Bindinig.User;
    using Models.Entities;
    using Tools;
    using Tools.Generic;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Common;
    using System.Data.Entity.Validation;
    using System.Text;
    using System.Xml.Schema;

    public class AuthenticationService : Service<UnitOfWork>
    {
        public IExecutionResult<User> Get(UserAuthenticationBindingModel model)
        {
            var repo = UnitOfWork.GetRepository<User>();

            var user = repo.FirstOrDefault(i => i.Password == model.Password && i.Email == model.Email);
            var execution = new ExecutionResult<User>();

            if (user == null)
            {
                execution.Succeeded = false;
                execution.Message = CrudMessages.NotFound("user");
            }
            else
            {
                execution.Result = user;
                execution.Succeeded = true;
                execution.Message = GlobalMessages.UserFound();
            }

            return execution;
        }

        public IExecutionResult Register(User user)
        {
            var execution = new ExecutionResult();

            try
            {
                user.IsAdministrator = false;
                UnitOfWork.GetRepository<User>().AddOrUpdate(user);
                UnitOfWork.Save();

                execution.Succeeded = true;
                execution.Message = GlobalMessages.SuccessfullyRegistered();
            }
            catch (DbEntityValidationException e)
            {
                var sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine($"{ve.PropertyName}: \"{ve.ErrorMessage}\"");
                    }
                }
                execution.Succeeded = false;
                execution.Message = sb.ToString();
            }
            catch (DbException e)
            {
                execution.Succeeded = false;
                execution.Message = e.Message;
            }
            catch (Exception e)
            {
                execution.Succeeded = false;
                execution.Message = GlobalMessages.InternalError();
            }

            return execution;
        }
    }
}
