﻿using BlackSound.Data.Repositories.Tools;
using BlackSound.Models;
using BlackSound.Models.Bindinig;
using BlackSound.Services.Tools;
using BlackSound.Services.Tools.Generic;

namespace BlackSound.Services
{
    public interface ICrudService<TEntity, TIndexViewModel, TBindingModel, TDetailsViewModel, TFilterBindingModel> where TEntity : class, IIdentificatable
    {
        IExecutionResult AddOrUpdate(TEntity item);
        IExecutionResult Delete(int id);
        IExecutionResult<IPager<TIndexViewModel>> GetAllFiltered(TFilterBindingModel filter);
        IExecutionResult<TDetailsViewModel> GetDetails(int id);
        IExecutionResult<TBindingModel> GetForModification(int id);
    }
}