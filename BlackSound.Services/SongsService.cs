﻿namespace BlackSound.Services
{
    using System.Linq;
    using Data.Repositories.Tools;
    using Data.UnitOfWork;
    using Models.Bindinig.Song;
    using Models.Entities;
    using Models.View.Song;
    using Tools.Generic;

    public class SongsService : CrudService<UnitOfWork, Song, SongIndexViewModel, SongBindingModel, SongDetailsViewModel, SongFilterBindingModel>
    {
        public IExecutionResult<IPager<SongIndexViewModel>> GetSongsInPlaylist(int playlistId, SongFilterBindingModel filter)
        {
            return _getAllFiltered(filter, where: i => i.Playlists.Any(p => p.PlaylistId == playlistId));
        }
    }
}
