﻿using BlackSound.Data.Repositories.Tools;

namespace BlackSound.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public interface IRepository<TEntity>
    {
        void AddOrUpdate(TEntity item);
        void Add(TEntity item);

        void Delete(TEntity item);

        IPager<TEntity> GetAllPaged(
            int page,
            int itemsPerPage,
            Expression<Func<TEntity, bool>> where = null);
        IPager<TResult> GetAllPaged<TResult>(int page,
            int itemsPerPage,
            Expression<Func<TEntity, bool>> where = null);

        ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> where = null);
        ICollection<TResult> GetAll<TResult>(Expression<Func<TEntity, bool>> where = null);

        TEntity FirstOrDefault(Func<TEntity, bool> where = null);
        TResult FirstOrDefault<TResult>(Expression<Func<TEntity, bool>> where = null);

        int Count();

        bool Any(Expression<Func<TEntity, bool>> any);
    }
}
