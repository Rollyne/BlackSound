﻿using System.Collections.Generic;

namespace BlackSound.Data.Repositories.Tools
{
    public interface IPager<TEntity>
    {
        int ItemsAvaliable { get; set; }
        ICollection<TEntity> Model { get; set; }
    }
}