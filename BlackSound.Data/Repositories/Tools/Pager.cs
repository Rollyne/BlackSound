﻿using System.Collections.Generic;

namespace BlackSound.Data.Repositories.Tools
{
    public class Pager<TEntity> : IPager<TEntity>
    {
        public ICollection<TEntity> Model { get; set; }
        public int ItemsAvaliable { get; set; }
    }
}
