﻿using System.Data.Entity;
using BlackSound.Data.Repositories;
using BlackSound.Models;

namespace BlackSound.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
        }

        public UnitOfWork()
        {
            _context = new BlackSoundContext();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IRepository<TEntity> GetRepository<TEntity>() 
            where TEntity : class, IIdentificatable
            => new Repository<TEntity>(_context);

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
