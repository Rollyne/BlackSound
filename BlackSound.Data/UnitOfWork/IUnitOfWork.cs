﻿using System;
using BlackSound.Data.Repositories;
using BlackSound.Models;

namespace BlackSound.Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>()
            where TEntity : class, IIdentificatable;

        void Save();
    }
}
