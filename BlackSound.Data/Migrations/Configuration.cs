namespace BlackSound.Data.Migrations
{
    using System.Data.Entity.Migrations;
    using Models.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<BlackSoundContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(BlackSoundContext context)
        {
            context.Users.AddOrUpdate(new User()
            {
                Id = 1,
                DisplayName = "Shefa",
                Email = "shefa@shefa.bg",
                IsAdministrator = true,
                Password = "shefa"
            });
        }
    }
}
