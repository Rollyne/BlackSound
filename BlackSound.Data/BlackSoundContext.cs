using BlackSound.Models.Entities;

namespace BlackSound.Data
{
    using System.Data.Entity;

    public class BlackSoundContext : DbContext
    {
        public BlackSoundContext()
            : base("name=BlackSound")
        {
        }
        
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Song> Songs { get; set; }

        public virtual DbSet<Playlist> Playlists { get; set; }
    }
}