﻿using BlackSound.Console.Menus;
using BlackSound.Console.Views.BaseViews;

namespace BlackSound.Console.Controllers
{
    public class HomeController
    {
        public void Index()
        {
            if (Engine.Context.User.IsAdministrator)
            {
                var option = new Index<AdminMainMenu>().Invoke();
                ProcessAdminMenu(option);
            }
            else
            {
                var option = new Index<UserMainMenu>().Invoke();
                ProcessUserMenu(option);
            }
            
        }

        public void ProcessUserMenu(int option)
        {
            var choice = (UserMainMenu)option;
            switch (choice)
            {
                case UserMainMenu.Songs:
                    new SongsController(true).Index();
                    break;
                case UserMainMenu.PlaylistManager:
                    new PlaylistsController().Index();
                    break;
                case UserMainMenu.Logout:
                    Engine.Context.Logout();
                    new AuthenticationController().Index();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Index();
                    break;
            }
        }

        private void ProcessAdminMenu(int option)
        {
            var choice = (AdminMainMenu) option;
            switch (choice)
            {
                case AdminMainMenu.UserManager:
                    new UsersController().Index();
                    break;
                case AdminMainMenu.SongManager:
                    new SongsController().Index();
                    break;
                case AdminMainMenu.PlaylistManager:
                    new PlaylistsController().Index();
                    break;
                case AdminMainMenu.Logout:
                    Engine.Context.Logout();
                    new AuthenticationController().Index();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Index();
                    break;
            }
        }
    }
}
