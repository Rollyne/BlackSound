﻿namespace BlackSound.Console.Controllers
{
    using Models.Bindinig.User;
    using Generic;
    using Data.UnitOfWork;
    using Models.Entities;
    using Models.View.User;
    using Services;

    public class UsersController : CrudController<CrudService<UnitOfWork, User, UserIndexViewModel, UserBindingModel, UserDetailsViewModel, UserFilterBindingModel>, User, UserIndexViewModel, UserBindingModel, UserDetailsViewModel, UserFilterBindingModel>
    {
        public UsersController(bool readAccessOnly = false) : base(readAccessOnly)
        {
        }
    }
}
