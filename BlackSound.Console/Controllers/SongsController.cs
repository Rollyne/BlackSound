﻿namespace BlackSound.Console.Controllers
{
    using Generic;
    using Data.UnitOfWork;
    using Models.Bindinig.Song;
    using Models.Entities;
    using Models.View.Song;
    using Services;
    public class SongsController : CrudController<SongsService, Song, SongIndexViewModel, SongBindingModel, SongDetailsViewModel, SongFilterBindingModel>
    {
        public SongsController(bool readAccessOnly = false) : base(readAccessOnly)
        {
        }
    }
}
