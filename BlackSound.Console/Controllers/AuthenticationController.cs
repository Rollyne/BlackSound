﻿namespace BlackSound.Console.Controllers
{
    using Generic;
    using Views.Authentication;
    using Menus;
    using Models.Bindinig.User;
    using Models.Entities;
    using Services;
    using Views.BaseViews;

    public class AuthenticationController : ServiceController<AuthenticationService>
    {
        public void Index()
        {
            var option = new Index<AuthenticationMenu>().Invoke();
            ProcessMenu(option);
        }

        public void Login(UserAuthenticationBindingModel model)
        {
            var execution = Engine.Context.Authenticate(model);
            Engine.Context.AddMessage(execution.Message);

            if (!execution.Succeeded)
            {
                Index();
            }
            else
            {
                new HomeController().Index();
            }
        }

        private void LoginHandler()
        {
            var model = new Login().Invoke();
            Login(model);
        }


        private void ProcessMenu(int option)
        {
            var choice = (AuthenticationMenu) option;

            switch (choice)
            {
                case AuthenticationMenu.Login:
                    LoginHandler();
                    break;
                case AuthenticationMenu.Register:
                    RegisterHandler();
                    break;
                case AuthenticationMenu.Exit:
                    Engine.Stop();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Index();
                    break;
            }
        }

        private void RegisterHandler()
        {
            var model = new Register().Invoke();
            Register(model);
        }

        public void Register(UserRegisterBindingModel model)
        {
            var user = AutoMapper.Mapper.Map<UserRegisterBindingModel, User>(model);
            var execution = Service.Register(user);

            Engine.Context.AddMessage(execution.Message);
            if (!execution.Succeeded)
            {
                Index();
            }
            else
            {
                Login(new UserAuthenticationBindingModel()
                {
                    Email = model.Email,
                    Password = model.Password
                });
            }
        }
    }
}
