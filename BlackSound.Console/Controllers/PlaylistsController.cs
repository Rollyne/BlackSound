﻿namespace BlackSound.Console.Controllers
{
    using System;
    using Generic;
    using Menus;
    using Models.Bindinig.Playlist;
    using Models.Bindinig.Song;
    using Models.Entities;
    using Models.View.Playlist;
    using Services;
    using Services.Tools;
    using Views.BaseViews;

    public class PlaylistsController : CrudController<PlaylistsService, Playlist, PlaylistIndexViewModel, PlaylistBindingModel, PlaylistDetailsViewModel, PlaylistFilterBindingModel>
    {
        public PlaylistsController(bool readAccessOnly = false) : base(readAccessOnly)
        {
        }

        protected override bool HasAccess(int id)
        {
            return Service.HasAccess(id, Engine.Context.User.Id);
        }

        protected override bool IsCreator(int id)
        {
            return Service.IsCreator(id, Engine.Context.User.Id);
        }

        protected override void ProcessMenu(int option)
        {
            var choice = (CrudMenu) option;
            switch (choice)
            {
                case CrudMenu.List:
                    List(new PlaylistFilterBindingModel()
                    {
                        UserId = Engine.Context.User.Id
                    });
                    break;
                case CrudMenu.Details:
                    Details(IdSelector());
                    break;
                case CrudMenu.Create:
                    CreateHandler();
                    break;
                case CrudMenu.Edit:
                    Edit(IdSelector());
                    break;
                case CrudMenu.Delete:
                    Delete(IdSelector());
                    break;
                case CrudMenu.Exit:
                    new HomeController().Index();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Index();
                    break;
            }
        }

        public override void ProcessDetailsMenu(int option, int currentPlaylistId)
        {
            var choice = (SongsManageMenu) option;
            switch (choice)
            {
                case SongsManageMenu.Add:
                    AddToPlaylistHandler(currentPlaylistId);
                    break;
                case SongsManageMenu.List:
                    ListFromPlaylist(currentPlaylistId, new SongFilterBindingModel());
                    break;
                case SongsManageMenu.Remove:

                    break;
                case SongsManageMenu.Exit:
                    Index();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Index();
                    break;
            }
        }

        public override void DetailsMenu(int currentId)
        {
            var option = new MenuPartial<SongsManageMenu>().Invoke();
            ProcessDetailsMenu(option, currentId);
        }

        public void AddToPlaylistHandler(int currentPlaylistId)
        {
            var songId = new IdSelector().Invoke();
            AddToPlaylist(songId, currentPlaylistId);
        }

        public void AddToPlaylist(int songId, int currentPlaylistId)
        {
            var service = new SongsPlaylistsService();
            //var isInPlaylistExecution = service.IsAlreadyInThePlaylist(currentPlaylistId, songId);
            //if (isInPlaylistExecution.Succeeded)
            //{
            //    if (isInPlaylistExecution.Result)
            //    {
            //        if (!new YesNoSelector($"{isInPlaylistExecution.Message} | Do you want to add it anyway? ").Invoke())
            //        {
            //            Details(currentPlaylistId);
            //        }
            //    }
            //}
            var execution = service.AddSongToPlaylist(songId, currentPlaylistId);
            Engine.Context.AddMessage(execution.Message);

            Details(currentPlaylistId);
        }

        public void ListFromPlaylist(int playlistId, SongFilterBindingModel filter)
        {
            if (filter.Page < 1)
            {
                Details(playlistId);
            }
            filter.ItemsPerPage = filter.ItemsPerPage < 1
                ? ApplicationConstants.DefaultItemsPerPage
                : filter.ItemsPerPage;
            var execution = new SongsService().GetSongsInPlaylist(playlistId, filter);
            if (!execution.Succeeded)
            {
                Details(playlistId);
            }
            var pagesAvaliable = (int)Math.Ceiling((double)execution.Result.ItemsAvaliable / filter.ItemsPerPage);
            new SongsController(true).Lister(filter, execution.Result, pagesAvaliable);


            ListFromPlaylistPager(pagesAvaliable, filter, playlistId);
        }
        public void ListFromPlaylistPager(int pagesAvaliable, SongFilterBindingModel filter, int currentPlaylistId)
        {
            var newFilter = new ListPager<SongFilterBindingModel>(pagesAvaliable, filter).Invoke();
            ListFromPlaylist(currentPlaylistId, newFilter);
        }

        public void RemoveSongFromPlaylist(int songId, int currentPlaylistId)
        {
            var execution = new SongsPlaylistsService().RemoveSongFromPlaylist(songId, currentPlaylistId);
            Engine.Context.AddMessage(execution.Message);

            Details(currentPlaylistId);
        }

        public override void Create(Playlist item)
        {
            item.CreatorId = Engine.Context.User.Id;
            base.Create(item);
        }

        public override IExecutionResult SendToEdit(Playlist item, int itemId)
        {
            item.CreatorId = Engine.Context.User.Id;
            return base.SendToEdit(item, itemId);
        }
    }
}
