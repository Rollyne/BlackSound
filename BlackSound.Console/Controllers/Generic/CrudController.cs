﻿
namespace BlackSound.Console.Controllers.Generic
{
    using System;
    using Data.Repositories.Tools;
    using Views.BaseViews;
    using Menus;
    using Models;
    using Models.Bindinig;
    using Services;
    using Services.Tools;

    public class CrudController<TService, TEntity, TIndexViewModel, TBindingModel, TDetailsViewModel, TFilterBindingModel> 
        : ServiceController<TService> 
        where TEntity : class, IIdentificatable
        where TBindingModel : new() 
        where TDetailsViewModel : new()
        where TFilterBindingModel : IPageFilter, new() 
        where TService : ICrudService<TEntity, TIndexViewModel, TBindingModel, TDetailsViewModel, TFilterBindingModel>, new()
    {
        protected virtual bool HasAccess(int id) => true;
        protected virtual bool IsCreator(int id) => true;
        private bool _readAccessOnly;
        public CrudController(bool readAccessOnly)
        {
            _readAccessOnly = readAccessOnly;
        }
        protected virtual void ProcessMenu(int option)
        {
            var choice = (CrudMenu) option;
            switch (choice)
            {
                case CrudMenu.List:
                    List(new TFilterBindingModel());
                    break;
                case CrudMenu.Details:
                    Details(IdSelector());
                    break;
                case CrudMenu.Create:
                    CreateHandler();
                    break;
                case CrudMenu.Edit:
                    Edit(IdSelector());
                    break;
                case CrudMenu.Delete:
                    Delete(IdSelector());
                    break;
                case CrudMenu.Exit:
                    new HomeController().Index();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Index();
                    break;
            }
        }

        protected void ProcessReadMenu(int option)
        {
            var choice = (ReadMenu)option;
            switch (choice)
            {
                case ReadMenu.List:
                    List(new TFilterBindingModel());
                    break;
                case ReadMenu.Details:
                    Details(IdSelector());
                    break;
                case ReadMenu.Exit:
                    new HomeController().Index();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Index();
                    break;
            }
        }
        

        public void Index()
        {
            int option;
            if (_readAccessOnly)
            {
                option = new Index<ReadMenu>(typeof(TEntity).Name).Invoke();
                ProcessReadMenu(option);
            }
            else
            {
                option = new Index<CrudMenu>(typeof(TEntity).Name).Invoke();
                ProcessMenu(option);
            }
        }

        public void List(TFilterBindingModel filter)
        {
            if (filter.Page < 1)
            {
                Index();
            }
            var execution = Service.GetAllFiltered(filter);
            if (!execution.Succeeded)
            {
                Engine.Context.AddMessage(execution.Message);
                Index();
                return;
            }
            filter.ItemsPerPage = filter.ItemsPerPage < 1
                ? ApplicationConstants.DefaultItemsPerPage
                : filter.ItemsPerPage;

            var pagesAvaliable = (int)Math.Ceiling((double)execution.Result.ItemsAvaliable / filter.ItemsPerPage);
            Lister(filter, execution.Result, pagesAvaliable);
            ListPager(pagesAvaliable, filter);
        }

        public void Lister(TFilterBindingModel filter, IPager<TIndexViewModel> pager, int pagesAvaliable)
        {
            filter.ItemsPerPage = ApplicationConstants.DefaultItemsPerPage;
            
            if (filter.Page > pagesAvaliable)
            {
                Engine.Context.AddMessage($"There are only {pagesAvaliable} pages avaliable.");
                filter.Page = 1;
            }

            new List<TIndexViewModel>(pager.Model).Invoke();
        }

        public void ListPager(int pagesAvaliable, TFilterBindingModel filter )
        {
            var newFilter = new ListPager<TFilterBindingModel>(pagesAvaliable, filter).Invoke();
            List(newFilter);
        }
        
        public virtual void Create(TEntity item)
        {
            var execution = Service.AddOrUpdate(item);
            Engine.Context.AddMessage(execution.Message);
            Index();
        }

        public void CreateHandler()
        {
            var model = new Create<TBindingModel>().Invoke();
            var item = AutoMapper.Mapper.Map<TBindingModel, TEntity>(model);
            Create(item);
        }

        public int IdSelector()
        {
            return new IdSelector().Invoke();
        }
        
        public void Edit(int id)
        {
            if (!IsCreator(id))
            {
                Engine.Context.AddMessage(CrudMessages.NoAccess(typeof(TEntity).Name.ToLower()));
                Index();
            }
            var execution = Service.GetForModification(id);

            if (!execution.Succeeded)
            {
                Engine.Context.AddMessage(execution.Message);
                Index();
                return;
            }

            var edited = new Edit<TBindingModel>(execution.Result).Invoke();

            var item = AutoMapper.Mapper.Map<TBindingModel, TEntity>(edited);
            var finalExecution = SendToEdit(item, id);

            Engine.Context.AddMessage(finalExecution.Message);
            Index();
        }

        public virtual IExecutionResult SendToEdit(TEntity item, int itemId)
        {
            item.Id = itemId;

            return Service.AddOrUpdate(item);
        }

        public void Delete(int id)
        {
            if (!IsCreator(id))
            {
                Engine.Context.AddMessage(CrudMessages.NoAccess(typeof(TEntity).Name.ToLower()));
                Index();
            }

            var execution = Service.GetForModification(id);

            if (!execution.Succeeded)
            {
                Engine.Context.AddMessage(execution.Message);
                Index();
                return;
            }

            if (new DeleteConfirm<TBindingModel>(execution.Result).Invoke())
            {
                var finalExecution = Service.Delete(id);
                Engine.Context.AddMessage(finalExecution.Message);
            }
            else
            {
                Engine.Context.AddMessage("Deletion cancelled.");
            }

            Index();
        }

        public void Details(int id)
        {
            if (!HasAccess(id))
            {
                Engine.Context.AddMessage(CrudMessages.NoAccess(typeof(TEntity).Name.ToLower()));
                Index();
            }
            var execution = Service.GetDetails(id);

            if (!execution.Succeeded)
            {
                Engine.Context.AddMessage(execution.Message);
                Index();
                return;
            }

            Engine.Context.AddMessage(execution.Message);
           new Details<TDetailsViewModel>(execution.Result).Invoke();
            DetailsMenu(id);
        }

        public virtual void DetailsMenu(int currentId)
        {

            var option = new MenuPartial<ExitMenu>().Invoke();
            ProcessDetailsMenu(option, currentId);
        }

        public virtual void ProcessDetailsMenu(int option, int currentId)
        {
            var choice = (ExitMenu) option;
            switch (choice)
            {
                case ExitMenu.Exit:
                    Index();
                    break;
                default:
                    Engine.Context.Messages.Add("Invalid command.");
                    Details(currentId);
                    break;
            }
        }
    }
}