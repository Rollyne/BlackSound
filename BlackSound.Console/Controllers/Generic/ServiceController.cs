﻿namespace BlackSound.Console.Controllers.Generic
{
    public class ServiceController<TService> 
        where TService : new()
    {
        protected TService Service;

        public ServiceController(TService service)
        {
            Service = service;
        }

        public ServiceController()
        {
            Service = new TService();
        }
        
    }
}
