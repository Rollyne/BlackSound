﻿namespace BlackSound.Console
{
    using Controllers;
    using System;

    public static class Engine
    {
        public static void Start()
        {
            Context = new Context();
        }

        public static void Stop()
        {
            Environment.Exit(0);
        }

        public static Context Context { get; set; }

        public static void LoadInitialView()
        {
            new AuthenticationController().Index();
        }
    }
}
