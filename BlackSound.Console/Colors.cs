﻿namespace BlackSound.Console
{
    using System;

    public static class Colors
    {
        public static ConsoleColor ApplicationTitle = ConsoleColor.DarkMagenta;
        public static ConsoleColor MenuName = ConsoleColor.Red;

        public static ConsoleColor MenuOptionNumber = ConsoleColor.Yellow;
        public static ConsoleColor MenuOptionText = ConsoleColor.Cyan;
        public static ConsoleColor HeaderLine = ConsoleColor.Blue;
        public static ConsoleColor MessagesHeader = ConsoleColor.Green;
        public static ConsoleColor Messages = ConsoleColor.Magenta;
        
        public static ConsoleColor CurrentValue = ConsoleColor.Green;
        public static ConsoleColor DistinctiveItems = ConsoleColor.Red;

        public static ConsoleColor PropertyName = ConsoleColor.Yellow;
        public static ConsoleColor InputText = ConsoleColor.Cyan;
        public static ConsoleColor InputOption = ConsoleColor.Green;

        public static ConsoleColor Table = ConsoleColor.Green;
        public static ConsoleColor TableHeader = ConsoleColor.Red;
        public static ConsoleColor TableValue = ConsoleColor.Cyan;

        public static ConsoleColor PagerHeader = ConsoleColor.Yellow;
        public static ConsoleColor PagerExit = ConsoleColor.Red;

    }
}
