﻿using BlackSound.Models.Bindinig.User;
using BlackSound.Models.Entities;
using BlackSound.Services;

namespace BlackSound.Console
{
    using System.Collections.Generic;
    using Services.Tools;
    public class Context
    {

        public User User { get; private set; }

        public Context()
        {
            Messages = new List<string>();
        }

        public ICollection<string> Messages { get; private set; }

        public void AddMessage(string message)
        {
            Messages.Add(message);
        }

        public void ClearMessages()
        {
            Messages = new List<string>();
        }

        public IExecutionResult Authenticate(UserAuthenticationBindingModel model)
        {
            var service = new AuthenticationService();

            var execution = service.Get(model);
            if (execution.Succeeded)
            {
                User = execution.Result;
            }

            return new ExecutionResult()
            {
                Message = execution.Message,
                Succeeded = execution.Succeeded
            };
        }

        public void Logout()
        {
            User = null;
        }
    }
}
