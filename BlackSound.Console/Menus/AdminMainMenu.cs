﻿namespace BlackSound.Console.Menus
{
    public enum AdminMainMenu
    {
        UserManager = 1,
        SongManager = 2,
        PlaylistManager = 3,
        Logout = 0

    }
}
