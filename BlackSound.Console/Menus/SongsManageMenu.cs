﻿namespace BlackSound.Console.Menus
{
    public enum SongsManageMenu
    {
        Add = 1,
        List = 2,
        Remove = 3,
        Exit = 0
    }
}
