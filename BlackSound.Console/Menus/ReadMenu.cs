﻿namespace BlackSound.Console.Menus
{
    public enum ReadMenu
    {
        List = 1,
        Details = 2,
        Exit = 0
    }
}
