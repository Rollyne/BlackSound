﻿namespace BlackSound.Console.Menus
{
    public enum AuthenticationMenu
    {
        Login = 1,
        Register = 2,
        Exit = 0
    }
}
