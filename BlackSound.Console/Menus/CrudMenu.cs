﻿namespace BlackSound.Console.Menus
{
    public enum CrudMenu
    {
        List = 1,
        Details = 2,
        Create = 3,
        Edit = 4,
        Delete = 5,
        Exit = 0,
    }
}
