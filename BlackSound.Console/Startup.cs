﻿using BlackSound.Models.Mapper;

namespace BlackSound.Console
{
    using System;

    public class Startup
    {
        public static void Main(string[] args)
        {
            Engine.Start();
            AutoMapperModelsConfiguration.Configure();
            Engine.LoadInitialView();
            Console.ReadKey();
        }
    }
}
