﻿namespace BlackSound.Console.Views
{
    public interface IInvocable
    {
        void Invoke();
    }
}
