﻿namespace BlackSound.Console.Views
{
    using System;

    public static class ViewHelper
    {
        public static void LoadLayout()
        {
            Console.Clear();
            Console.ForegroundColor = Colors.ApplicationTitle;
            Console.WriteLine("Black Sound Console Application");
            Console.ForegroundColor = Colors.HeaderLine;
            Console.WriteLine(new string('_', 30));

            if (Engine.Context.Messages.Count > 0)
            {
                Console.ForegroundColor = Colors.MessagesHeader;
                Console.WriteLine("Messages:");
                Console.ForegroundColor = Colors.Messages;
                foreach (var message in Engine.Context.Messages)
                {
                    Console.WriteLine(message);
                }

                Engine.Context.ClearMessages();
                Console.ForegroundColor = Colors.HeaderLine;
                Console.WriteLine(new string('_', 30));
            }

            Console.ResetColor();
        }

        public static string GetEntityName(Type typeOfModel, string textToRemove)
        {
            var modelName = typeOfModel.Name;
            var startIndex = modelName.IndexOf(textToRemove, StringComparison.Ordinal);
            
            var entityName = startIndex < 1 ? modelName : modelName.Remove(startIndex, textToRemove.Length);

            return entityName;
        }

        public static int MenuGenerator<TEnumMenu>(string menuName = null)
        {
            var mName = menuName ?? GetEntityName(typeof(TEnumMenu), "Menu");

            Console.ForegroundColor = Colors.MenuName;
            Console.WriteLine($"{mName} Menu Options");
            var menuValues = Enum.GetValues(typeof(TEnumMenu));
            foreach (var item in menuValues)
            {
                Console.ForegroundColor = Colors.PropertyName;
                Console.Write($"{(int)item}. ");
                Console.ForegroundColor = Colors.InputText;
                Console.WriteLine(item);
            }

            Console.WriteLine();

            Console.ForegroundColor = Colors.InputOption;
            Console.Write("> ");


            if (!int.TryParse(Console.ReadLine(), out int choice))
            {
                choice = -1;
            }

            return choice;
        }
    }
}
