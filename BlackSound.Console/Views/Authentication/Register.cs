﻿namespace BlackSound.Console.Views.Authentication
{
    using System;
    using Generic;
    using Models.Bindinig.User;
    public class Register : LayoutView,  IInvocable<UserRegisterBindingModel>
    {
        public UserRegisterBindingModel Invoke()
        {
            Console.ForegroundColor = Colors.MenuName;
            Console.WriteLine("Register a new user");
            var propertiesInfo = typeof(UserRegisterBindingModel).GetProperties();
            var item = new UserRegisterBindingModel();

            foreach (var prop in propertiesInfo)
            {
                Console.ForegroundColor = Colors.PropertyName;
                Console.Write($"{prop.Name}: ");
                Console.ForegroundColor = Colors.InputText;
                prop.SetValue(item, Convert.ChangeType(Console.ReadLine(), prop.PropertyType));
            }

            return item;
        }
    }
}
