﻿

namespace BlackSound.Console.Views.Authentication
{
    using System;
    using Generic;
    using Models.Bindinig.User;

    public class Login : LayoutView,  IInvocable<UserAuthenticationBindingModel>
    {
        public UserAuthenticationBindingModel Invoke()
        {
            Console.ForegroundColor = Colors.MenuName;
            Console.WriteLine("Login");
            var propertiesInfo = typeof(UserAuthenticationBindingModel).GetProperties();
            var item = new UserAuthenticationBindingModel();

            foreach (var prop in propertiesInfo)
            {
                Console.ForegroundColor = Colors.PropertyName;
                Console.Write($"{prop.Name}: ");
                Console.ForegroundColor = Colors.InputText;
                prop.SetValue(item, Convert.ChangeType(Console.ReadLine(), prop.PropertyType));
            }

            return item;
        }
    }
}
