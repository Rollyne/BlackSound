﻿namespace BlackSound.Console.Views.Generic
{
    public interface IInvocable<TResult>
    {
        TResult Invoke();
    }
}
