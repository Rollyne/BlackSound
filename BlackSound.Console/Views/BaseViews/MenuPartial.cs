﻿namespace BlackSound.Console.Views.BaseViews
{
    using Generic;
    using System;
    public class MenuPartial<TEnumMenu> : IInvocable<int>
    {
        public int Invoke()
        {
            Console.WriteLine();
            Console.ForegroundColor = Colors.HeaderLine;
            Console.WriteLine(new string('_', 30));
            return ViewHelper.MenuGenerator<TEnumMenu>();
        }
    }
}
