﻿namespace BlackSound.Console.Views.BaseViews
{
    using System;
    using Generic;
    using Models.Bindinig;
    public class ListPager<TFilterBindingModel> : IInvocable<TFilterBindingModel>
        where TFilterBindingModel : IPageFilter
    {

        private TFilterBindingModel _model;
        private readonly int _pagesAvaliable;
        public ListPager(int pagesAvaliable, TFilterBindingModel filter)
        {
            _model = filter;
            _pagesAvaliable = pagesAvaliable;
        }
        public TFilterBindingModel Invoke()
        {
            Console.ForegroundColor = Colors.PagerHeader;
            Console.WriteLine($"Page {_model.Page} from {_pagesAvaliable}");
            Console.ForegroundColor = Colors.PagerExit;
            Console.Write("|Exit - 0|");
            Console.ForegroundColor = Colors.InputOption;
            Console.Write(" Go to page > ");

            if (!int.TryParse(Console.ReadLine(), out int pageSelected))
            {
                pageSelected = 0;
            }
            _model.Page = pageSelected;

            return _model;
        }
    }
    
}
