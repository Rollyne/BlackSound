﻿namespace BlackSound.Console.Views.BaseViews
{
    using System;
    using Generic;

    public class Create<TBindingModel> : LayoutView, IInvocable<TBindingModel> 
        where TBindingModel : new()
    {
        public TBindingModel Invoke()
        {
            var entityName = ViewHelper.GetEntityName(typeof(TBindingModel), "BindingModel");

            Console.ForegroundColor = Colors.MenuName;
            Console.WriteLine($"Create new {entityName.ToLower()}");
            var propertiesInfo = typeof(TBindingModel).GetProperties();
            var item = new TBindingModel();

            foreach (var prop in propertiesInfo)
            {
                Console.ForegroundColor = Colors.PropertyName;
                Console.Write($"{prop.Name}: ");
                try
                {
                    Console.ForegroundColor = Colors.InputText;
                    prop.SetValue(item, Convert.ChangeType(Console.ReadLine(), prop.PropertyType));
                }
                catch (Exception)
                {
                    Engine.Context.AddMessage($"Invalid input for {entityName.ToLower()}");
                }
                
            }

            return item;
        }
    }
}
