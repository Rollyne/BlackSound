﻿namespace BlackSound.Console.Views.BaseViews
{
    using System;
    using Generic;

    public class Edit<TBindingModel> : LayoutView, IInvocable<TBindingModel> where TBindingModel : new()
    {
        private readonly TBindingModel _model;

        public Edit(TBindingModel model)
        {
            _model = model;
        }

        public TBindingModel Invoke()
        {
            var entityName = ViewHelper.GetEntityName(typeof(TBindingModel), "BindingModel");

            Console.ForegroundColor = Colors.MenuName;
            Console.WriteLine($"Update {entityName.ToLower()}");
            var propertiesInfo = typeof(TBindingModel).GetProperties();
            var item = new TBindingModel();
            foreach (var prop in propertiesInfo)
            {
                Console.ForegroundColor = Colors.PropertyName;
                Console.Write($"{prop.Name} ");
                Console.ForegroundColor = Colors.CurrentValue;
                Console.Write($"|{prop.GetValue(_model)}|");
                Console.ForegroundColor = Colors.PropertyName;
                Console.Write(": ");
                Console.ForegroundColor = Colors.InputText;
                try
                {
                    prop.SetValue(item, Convert.ChangeType(Console.ReadLine(), prop.PropertyType));
                }
                catch (Exception)
                {
                    Engine.Context.AddMessage($"Invalid input for {entityName.ToLower()}");
                }
            }

            return item;
        }
    }
}
