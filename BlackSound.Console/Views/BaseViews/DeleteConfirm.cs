﻿namespace BlackSound.Console.Views.BaseViews
{
    using System;
    using Generic;

    public class DeleteConfirm<TBindingModel> : LayoutView, IInvocable<bool> where TBindingModel : new()
    {
        private readonly TBindingModel _model;
        public DeleteConfirm(TBindingModel model)
        {
            _model = model;
        }
        public bool Invoke()
        {
            var entityName = ViewHelper.GetEntityName(typeof(TBindingModel), "BindingModel");

            Console.ForegroundColor = Colors.MenuName;
            Console.WriteLine($"Are you sure that you want to delete this {entityName.ToLower()}?");

            var propertiesInfo = typeof(TBindingModel).GetProperties();
            foreach (var prop in propertiesInfo)
            {
                Console.ForegroundColor = Colors.PropertyName;
                Console.WriteLine($"{prop.Name}: ");
                Console.ForegroundColor = Colors.InputText;
                Console.WriteLine(prop.GetValue(_model));
            }

            Console.ForegroundColor = Colors.InputOption;
            Console.Write("(Y/N)> ");
            var answer = Console.ReadLine();
            answer = string.IsNullOrEmpty(answer) ? "N" : answer;
            return answer[0] == 'Y';
        }
    }
}
