﻿

namespace BlackSound.Console.Views.BaseViews
{
    using System;
    using Generic;
    public class Index<TEnumMenu> : LayoutView, IInvocable<int>
    {
        private readonly string _menuName;
        public Index(string menuName = null)
        {
            _menuName = menuName;
        }
        public int Invoke()
        {
            return ViewHelper.MenuGenerator<TEnumMenu>(_menuName);
        }
    }
}
