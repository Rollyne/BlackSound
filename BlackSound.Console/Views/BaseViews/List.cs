﻿namespace BlackSound.Console.Views.BaseViews
{
    using System.Collections.Generic;
    using System;
    public class List<TIndexViewModel> : LayoutView, IInvocable
    {
        private readonly ICollection<TIndexViewModel> _model;
        public List(ICollection<TIndexViewModel> model)
        {
            _model = model;
        }
        public void Invoke()
        {
            
            var entityName = ViewHelper.GetEntityName(typeof(TIndexViewModel), "IndexViewModel");

            Console.ForegroundColor = Colors.MenuName;
            Console.WriteLine($"List of all {entityName.ToLower()}s");
            Console.ForegroundColor = Colors.HeaderLine;
            Console.WriteLine(new string('_', 100));

            var propertiesInfo = typeof(TIndexViewModel).GetProperties();
            foreach (var item in propertiesInfo)
            {
                Console.ForegroundColor = Colors.TableHeader;
                Console.Write(item.Name.PadLeft(item.Name.Length + 15));
                Console.ForegroundColor = Colors.TableHeader;
                Console.Write(" |");
            }

            Console.WriteLine();

            foreach (var item in _model)
            {
                foreach (var prop in propertiesInfo)
                {
                    Console.ForegroundColor = Colors.TableValue;
                    Console.Write(prop.GetValue(item).ToString().PadLeft(prop.Name.Length + 15));
                    Console.ForegroundColor = Colors.Table;
                    Console.Write(" |");
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }
    }
}
