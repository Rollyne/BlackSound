﻿namespace BlackSound.Console.Views.BaseViews
{
    using Generic;
    using System;
    public class IdSelector : LayoutView, IInvocable<int>
    {
        public int Invoke()

        {
            Console.ForegroundColor = Colors.PropertyName;
            Console.Write("Enter the ");
            Console.ForegroundColor = Colors.DistinctiveItems;
            Console.Write("id");
            Console.ForegroundColor = Colors.PropertyName;
            Console.Write(" of the target item: ");
            Console.ForegroundColor = Colors.InputText;
            if (!int.TryParse(Console.ReadLine(), out int id))
            {
                id = -1;
            }
            return id;
        }
    }
}
