﻿namespace BlackSound.Console.Views.BaseViews
{
    using System;
    using Generic;

    public class YesNoSelector : LayoutView, IInvocable<bool>
    {
        private readonly string _message;
        public YesNoSelector(string message)
        {
            _message = message;
        }
        public bool Invoke()
        {
            Console.ForegroundColor = Colors.PropertyName;
            Console.Write(_message);
            Console.ForegroundColor = Colors.InputOption;
            Console.Write(" (Y/N)> ");
            var answer = Console.ReadLine();
            answer = string.IsNullOrEmpty(answer) ? "N" : answer;
            return answer[0] == 'Y';
        }
    }
}
