﻿namespace BlackSound.Console.Views.BaseViews
{
    using System;
    public class Details<TDetailsViewModel> : LayoutView, IInvocable where TDetailsViewModel : new()
    {
        private readonly TDetailsViewModel _model;

        public Details(TDetailsViewModel model)
        {
            _model = model;
        }
        public void Invoke()
        {
            var propertiesInfo = typeof(TDetailsViewModel).GetProperties();
            var item = new TDetailsViewModel();
            foreach (var prop in propertiesInfo)
            {
                Console.ForegroundColor = Colors.PropertyName;
                Console.Write($"{prop.Name}: ");
                Console.ForegroundColor = Colors.InputText;
                Console.WriteLine($"{prop.GetValue(_model)}");
            }
        }
    }
}
