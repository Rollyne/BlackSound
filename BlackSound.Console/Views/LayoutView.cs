﻿namespace BlackSound.Console.Views
{
    public abstract class LayoutView
    {
        protected LayoutView()
        {
            ViewHelper.LoadLayout();
        }
    }
}
